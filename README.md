## missi_phoneext4_global-user 13 TKQ1.221114.001 V14.0.2.0.TGKMIXM release-keys
- Manufacturer: xiaomi
- Platform: bengal
- Codename: spes
- Brand: Redmi
- Flavor: missi_phoneext4_global-user
- Release Version: 13
- Kernel Version: 4.19.157
- Id: TKQ1.221114.001
- Incremental: V14.0.2.0.TGKMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: Redmi/spes_global/spes:13/TKQ1.221114.001/V14.0.2.0.TGKMIXM:user/release-keys
- OTA version: 
- Branch: missi_phoneext4_global-user-13-TKQ1.221114.001-V14.0.2.0.TGKMIXM-release-keys
- Repo: redmi_spes_dump
